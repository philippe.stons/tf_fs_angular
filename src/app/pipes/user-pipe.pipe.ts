import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../models/user.model'

@Pipe({
  name: 'userPipe',
})
export class UserPipe implements PipeTransform {

  transform(users: User[]) {
    return users.filter(user => user.active);
  }
}
