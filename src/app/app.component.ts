import { Component, OnInit, OnDestroy } from '@angular/core';
import { interval, Subscription } from 'rxjs'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: string = 'tf fs web';
  date: Date = new Date();
  
  constructor()
  {}

  ngOnInit(): void
  {
  }

  //Called once, before the instance is destroyed.
  //Add 'implements OnDestroy' to the class.
  ngOnDestroy(): void {
  }
}
