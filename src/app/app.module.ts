import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http';

import { UserService } from './services/user.service';
import { ProductService } from './services/product.service';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard.service';
import { YoutubeService } from './services/youtube.service';

import { AppComponent } from './app.component';
import { ProductComponent } from './components/product/product.component';
import { UsersComponent } from './components/users/users.component';
import { UserviewComponent } from './views/userview/userview.component';
import { ProductviewComponent } from './views/productview/productview.component'
import { SingleUserComponent } from './views/single-user/single-user.component';
import { SingleProductComponent } from './views/single-product/single-product.component';
import { AuthComponent } from './components/auth/auth.component';
import { EditUserViewComponent } from './views/edit-user-view/edit-user-view.component';
import { YoutubeComponent } from './components/youtube/youtube.component';
import { UserPipe } from './pipes/user-pipe.pipe';
import { YoutubeViewComponent } from './views/youtube-view/youtube-view.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    UsersComponent,
    UserviewComponent,
    ProductviewComponent,
    SingleUserComponent,
    SingleProductComponent,
    AuthComponent,
    EditUserViewComponent,
    YoutubeComponent,
    UserPipe,
    YoutubeViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    UserService,
    ProductService,
    AuthService,
    AuthGuardService,
    YoutubeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
