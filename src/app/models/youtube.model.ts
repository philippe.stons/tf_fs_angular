export class YoutubeVideo
{
    videoId: string;
    videoUrl: string;
    channelId: string;
    channelUrl: string;
    channelTitle: string;
    title: string;
    publishedAt: Date;
    description: string;
    thumbnail: string;

    constructor(data: any)
    {
        this.videoId = data.videoId;
        this.videoUrl = data.videoUrl;
        this.channelId = data.channelId;
        this.channelUrl = data.channelUrl;
        this.channelTitle = data.channelTitle;
        this.title = data.title;
        this.publishedAt = data.publishedAt;
        this.description = data.description;
        this.thumbnail = data.thumbnail;
    }
}