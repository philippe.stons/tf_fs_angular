export class User
{
    public id: number;
    public username: string;
    public password: string;
    public active: boolean;

    constructor(data: any)
    {
        this.id = data.id;
        this.username = data.username;
        this.password = data.password;
        this.active = data.active;
    }
}