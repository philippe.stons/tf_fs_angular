import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService} from '../../services/auth.service'

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})

export class AuthComponent implements OnInit {
  signButton!: string;
  authStatus!: boolean;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.updateButtonText();
  }

  onClick()
  {
    console.log('click ');
    if(this.authService.isAuth)
    {
      this.authService.signOut();
    } else 
    {
      this.authService.signIn().then(
        () => 
        {
          console.log('Sign in successful!');
          this.authStatus = this.authService.isAuth;
          this.router.navigate(['products']);
        }
      );
    }
    this.updateButtonText();
  }

  updateButtonText(): void
  {
    this.authStatus = this.authService.isAuth;
    console.log(this.authService.isAuth);
    if(this.authService.isAuth)
    {
      this.signButton = 'sign out';
    } else 
    {
      this.signButton = 'sign in';
    }
  }

}
