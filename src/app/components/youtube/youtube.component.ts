import { Component, Input, OnInit } from '@angular/core';
import { YoutubeVideo } from 'src/app/models/youtube.model';

@Component({
  selector: 'app-youtube',
  templateUrl: './youtube.component.html',
  styleUrls: ['./youtube.component.scss']
})
export class YoutubeComponent implements OnInit {
  @Input() video!: YoutubeVideo;

  constructor() {}

  ngOnInit(): void {
  }

}
