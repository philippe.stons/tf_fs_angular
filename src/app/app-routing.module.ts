import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductviewComponent } from './views/productview/productview.component';
import { UserviewComponent } from './views/userview/userview.component';
import { SingleUserComponent } from './views/single-user/single-user.component';
import { SingleProductComponent } from './views/single-product/single-product.component';
import { AuthComponent } from './components/auth/auth.component';
import { AuthGuardService } from './services/auth-guard.service';
import { EditUserViewComponent } from './views/edit-user-view/edit-user-view.component';
import { YoutubeViewComponent } from './views/youtube-view/youtube-view.component';

const routes: Routes = 
[
    { path: 'products', canActivate: [AuthGuardService], component: ProductviewComponent },
    { path: 'products/:id', canActivate: [AuthGuardService], component: SingleProductComponent },
    { path: 'users', canActivate: [AuthGuardService], component: UserviewComponent },
    { path: 'users/:id', canActivate: [AuthGuardService], component: SingleUserComponent },
    { path: 'edit_user', canActivate: [AuthGuardService], component: EditUserViewComponent },
    { path: 'youtube', canActivate: [AuthGuardService], component: YoutubeViewComponent },
    { path: 'auth', component: AuthComponent },
    { path: '', component: UserviewComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
