import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../../services/user.service'
import { User } from '../../models/user.model'
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-userview',
  templateUrl: './userview.component.html',
  styleUrls: ['./userview.component.scss']
})
export class UserviewComponent implements OnInit {
  userSub!: Subscription;
  users!: User[]; 

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userSub = this.userService.userSubject.subscribe(
      (users: User[]) =>
      {
        this.users = users;
      }
    );
    this.userService.emitUsersSubject();
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
  }

}
