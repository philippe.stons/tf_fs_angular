import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, ValidatorFn } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-edit-user-view',
  templateUrl: './edit-user-view.component.html',
  styleUrls: ['./edit-user-view.component.scss']
})
export class EditUserViewComponent implements OnInit {
  userForm!: FormGroup;
  usernameCtl!: FormControl;
  passwordCtl!: FormControl;

  constructor(private userService: UserService, private router: Router, private formBuilder: FormBuilder) 
  {
    this.initForm();
  }

  ngOnInit(): void {
  }

  initForm(): void
  {
    this.usernameCtl = this.formBuilder.control('', [Validators.required, Validators.minLength(5)]);
    this.passwordCtl = this.formBuilder.control('', [Validators.required, Validators.minLength(5), this.checkPassword()]);

    this.userForm = this.formBuilder.group({
      username: this.usernameCtl,
      password: this.passwordCtl
    });
  }

  checkPassword(): ValidatorFn
  {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const password = control.value;
      /*
      if(password === this.usernameCtl.value)
      {
        return true
      }
      return null;
      */
      return password === this.usernameCtl.value 
        && (!control.hasError('required')) ? 
          {forbidden: {value: 'password and username are equals'}} : null;
    };
  }

  onSubmit()
  {
    const formVal = this.userForm.value;
    
    console.log("username : " + formVal.username + " password : " + formVal.password);
    const newUser = new User(formVal);

    this.userService.addUser(newUser);
    this.router.navigate(['/users']);
  }

}
