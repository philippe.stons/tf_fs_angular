import { Component, OnInit } from '@angular/core';
import { YoutubeService } from 'src/app/services/youtube.service';
import { YoutubeVideo } from '../../models/youtube.model'

@Component({
  selector: 'app-youtube-view',
  templateUrl: './youtube-view.component.html',
  styleUrls: ['./youtube-view.component.scss']
})
export class YoutubeViewComponent implements OnInit {
  videos: YoutubeVideo[];
  hasContent: boolean = false;
  query: string = '';

  constructor(private youtubeService: YoutubeService) {
    this.videos = [];
  }

  ngOnInit(): void {
  }

  searchVideo(): void
  {
    if(this.query != '')
    {
      this.youtubeService.getVideos(this.query).subscribe((items: any) => {
        this.videos = items.map((item: any) => {
          return {
            title: item.snippet.title,
            videoId: item.id.videoId,
            videoUrl: `https://www.youtube.com/watch?v=${item.id.videoId}`,
            channelId: item.snippet.channelId,
            channelUrl: `https://www.youtube.com/channel/${item.snippet.channelId}`,
            channelTitle: item.snippet.channelTitle,
            description: item.snippet.description,
            publishedAt: new Date(item.snippet.publishedAt),
            thumbnail: item.snippet.thumbnails.high.url
          };
        });
      });
    }
  }

}
