import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
  })
export class AuthService {
    isAuth: boolean = true; // TODO reset to false

    signIn()
    {
        return new Promise(
            (resolve, reject) => 
            {
                setTimeout(
                    () => 
                    {
                        this.isAuth = true;
                        resolve(true);
                    }, 500
                );
            }
        );
    }

    signOut()
    {
        this.isAuth = false;
    }
}
