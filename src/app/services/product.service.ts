import { Injectable } from '@angular/core';
import { Product } from '../models/product.model';

@Injectable()
export class ProductService {
    private products: Product[] = [
        {
          id: 0,
          title: 'produt 1',
          description: 'an expensive product',
          price: 1000,
          stockQuantity: 0
        },
        {
          id: 1,
          title: 'produt 2',
          description: 'an expensive product',
          price: 1000,
          stockQuantity: 11
        },
        {
          id: 2,
          title: 'produt 3',
          description: 'an cheap product',
          price: 2,
          stockQuantity: 1000
        }
      ];
  
  constructor() { }

  getProducts(): Product[]
  {
    return this.products;
  }

  getOneProductByID(id: number): Product
  {
    return this.products[id];
  }
}
