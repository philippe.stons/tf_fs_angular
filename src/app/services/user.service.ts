import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { User } from '../models/user.model'

@Injectable()
export class UserService {
  userSubject = new Subject<User[]>();
  private users: User[] = [
      {
        id: 0,
        username: 'root',
        password: 'password',
        active: true
      },
      {
        id: 1,
        username: 'phil',
        password: 'password2',
        active: true
      }
    ];
  
  constructor() { }

  emitUsersSubject()
  {
    this.userSubject.next(this.users.slice());
  }

  getUsers(): User[]
  {
    return this.users;
  }

  getOneUserByID(id: number): User
  {
    return this.users[id];
  }

  addUser(user: User)
  {
    user.id = this.users.length;
    this.users.push(user);
    this.emitUsersSubject();
  }
}
